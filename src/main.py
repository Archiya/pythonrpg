from pygame import init, display, event, time, quit
from pygame import QUIT, KEYDOWN, K_UP, K_DOWN, K_LEFT, K_RIGHT, K_w, K_s, K_a, K_d, K_RETURN, K_ESCAPE
#from resources.LevelTileManager import LevelTileManager
from resources.EventBus import EventBus
from resources.frameworks.Event import Event
import sys
import time as osTime


class Runtime:
    def __init__(self):
        init()
        display.set_caption("Test Project")
        self.fps = 30
        self.tileResolution = 32
        self.screenWidth = len(testLevel[0]) * self.tileResolution
        self.screenHeight = len(testLevel) * self.tileResolution
        print(self.screenWidth, self.screenHeight)
        self.screen = display.set_mode((self.screenWidth, self.screenHeight))
        self.clock = time.Clock()
        self.eventBus = EventBus()
        #self.levelManager = LevelTileManager(level=testLevel, row=len(testLevel[0]), col=len(testLevel))

    def draw(self):
        self.eventBus.send(Event(fromManager="Runtime", toManager="CompositionManager", id="idkyet", request="Draw"))
        #self.levelManager.draw(self.screen)
        display.flip()
    
    def quit(self):
        quit()
        sys.exit()

    def events(self):
        for action in event.get():
            if action.type == QUIT:
                self.quit()
            if action.type == KEYDOWN:
                if action.key == K_ESCAPE:
                    self.quit()
                #if action.key == 'Q':
                    #self.manager.SendAction('<playerManager>, <id>, <ignite>')
                    #self.manager.SendAction('<inventoryManager>, <id>, <ignite>')
                    #self.manager.SendAction('<levelTileManager>, <id>, <ignite>')


    def run(self) -> bool:
        self.playing = True
        while self.playing:
            dt = self.clock.tick(self.fps) / 1000
            self.events()
            self.draw()


def main():
    game = Runtime()
    game.run()

if __name__=='__main__':
    main()



