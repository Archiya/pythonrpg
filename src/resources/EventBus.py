from re import L
from frameworks.Event import Event
from managers.CompositionManager import CompositionManager

class EventBus():
    def __init__(self):
        self.compositionManager = CompositionManager()
        # self.eventManager = EventManager()
        # self.playerManager = PlayerManager()
        # self.aiManager = AIManager()
        # self.uiManager = UIManager()
    
    def send(self, event: Event):
        if (event.toManager == "CompositionManager"):
            self.compositionManager.receive(event)
        # elif (event.toManager == "EventManager"):
        #     self.eventManager.receive(event)
        # elif (event.toManager == "PlayerManager"):
        #     self.playerManager.receive(event)
        # elif (event.toManager == "AIManager"):
        #     self.aiManager.receive(event)
        # elif (event.toManager == "UIManager"):
        #     self.uiManager.receive(event)


    