from Layer import Layer

class Event:
    def __init__(self, fromManager: str, toManager: str, id: str, request: str, layer: Layer = None, msg: dict = None):
        self.fromManager = fromManager
        self.toManager = toManager
        self.id = id
        self.request = request
        self.layer = layer
        self.msg = msg 
