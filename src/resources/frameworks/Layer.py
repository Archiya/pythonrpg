from dataclasses import dataclass
from typing import List

@dataclass
class Layer():
    def __init__(self, grid: List[List[int]], row: int = 0, col: int = 0):
        self.grid = grid
        self.row = row
        self.col = col