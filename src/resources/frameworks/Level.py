from Layer import Layer
from typing import List
from ..maps import TestLevels as test

class Level():
    def __init__(self, prebuiltLevel: str = "None", level: List[List[int]] = [[]]):
        #optionally use prebuilt levels
        if (prebuiltLevel == "testLevel"):
            level = test.testLevel
        self.layer = Layer(level, row=len(level[0]), col=len(level))
