import yaml
import os
from pygame import image

class ImageResource():
    def __init__(self, dataFile: str = 'resource.yaml', metaFile: str = 'resourceMetadata.yaml', res: str = 'Unknown', frames: int = 0):
        self.homePath = os.path.dirname(os.path.realpath(__file__))
        self.defaultConfigurationFolder = "configuration"
        with open(os.path.join(self.homePath, self.defaultConfigurationFolder, metaFile)) as metaFile:
        #with open(os.path.join("configuration/resourceMetdata.yaml")) as metaFile:
            meta = yaml.full_load(metaFile)
            self.width = meta[res]['width']
            self.height = meta[res]['height']
        with open(os.path.join(self.homePath, self.defaultConfigurationFolder, dataFile)) as resFile: 
            data = yaml.full_load(resFile)
            self.imagePathBuffer = []
        for frame in data[res]:
            imagePath = os.path.join(self.homePath, data['directory'], frame)
            self.imagePathBuffer.append(imagePath)
            # ['/C/asd/asd/asd/', '/C/asd/asd/asd/']

