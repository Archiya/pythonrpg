from dataclasses import dataclass
from pygame import image

@dataclass
class Point:
    '''x/y coordinates for point on screen.'''
    x: int
    y: int
    def __init__(self, x: int = 0, y: int = 0):
        self.x = x
        self.y = y 

class Moveable:
    '''Any object that can be moved.'''
    def __init__(self, spriteMap: list, currentFrame: int = 0, pos: Point = Point()):
        '''
            @spriteMap: list of image paths that represent the sprite
            @currentFrame: can be overriden if you want to start later in the animation
            @pos: (x, y), defaults to (0,0)
        '''
        self.pos = pos
        self.currentFrame = currentFrame
        self.frameBuffer = []
        for frame in spriteMap:
            #['/asdasd/asdasd', '/asdasd/asdasd']
            currentFrame = image.load(frame)
            self.frameBuffer.append(currentFrame)
            #['pygame<sdfsd>', 'pygame<qweqwe>']
    
    def draw(self, screen) -> bool:
        '''
            @screen: pygame.screen object to be drawn to
            @ret: False if the screen wasn't written to successfully
        '''
        try:
            screen.blit(self.frameBuffer[self.currentFrame], (self.pos.x, self.pos.y))
            self.currentFrame = (self.currentFrame + 1) % len(self.frameBuffer)
        except Exception as e:
            print(f'failed to draw to screen.')
            return False
        return True
    
    def move(self, xDelta, yDelta) -> Point:
        '''
            @xOffset: x offset value
            @yOffset: y offset value
            @ret: updated point representing the upper x/y of the sprite
        '''
        self.pos.x = self.pos.x + xDelta
        self.pos.y = self.pos.y + yDelta
        return self.pos


'''
    Layer Manager
        Level Tile Manager
        Set Design Manager
        Building Manager
        Character Sprite Manager
'''


'''
    ------------------------
    ----------------|---|---
    --{---}-----------x-----
    ------------------------
'''

