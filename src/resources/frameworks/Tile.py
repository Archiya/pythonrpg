from .Sprite import Moveable
from .Sprite import Point

class Tile(Moveable):
    '''Abstraction for the tiles the represent the game map.'''
    def __init__(self, spriteMap: list, currentFrame: int = 0, pos: Point = Point(), height: int = 32, width: int = 32):
        '''
            @height: the offset from the upper left corner to the bottom left corner.
            @width: the offset from the upper left corner to the upper right corner.
        '''
        Moveable.__init__(self, spriteMap, currentFrame, pos)
        self.height = height
        self.width = width