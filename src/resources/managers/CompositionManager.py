from .Manager import Manager
from .LevelTileManager import LevelTileManager
from ..EventBus import EventBus
from ..frameworks import Event

class CompositionManager(Manager):
    def __init__(self, eventBus: EventBus):
        self.eventBus = eventBus
        self.levelTileManager = LevelTileManager()

    def send(self, event: Event):
        self.eventBus.send(event)

    def receive(self, event: Event):
        if (event.request == "ChangeLevel"):
            self.levelTileManager.changeLevel(event.msg['levelName'], event.layer)
        if (event.request == "Draw"):
