from ..frameworks.Tile import Tile
from ..frameworks.Sprite import Point
from ..frameworks.ResourceLookup import ImageResource
from ..frameworks.Level import Level

from typing import List

class LevelTileManager():
    def __init__(self):
        self.level = Level(prebuiltLevel="testLevel")
        self.renderLevel()

    def changeLevel(self, prebuiltLevel: str = "testLevel", level: List[List[int]] = [[]]):
        self.level = Level(prebuiltLevel, level)
        self.renderLevel()

    def renderLevel(self):
        for y in range(self.level.layer.col):
            for x in range(self.level.layer.row):
                res = ImageResource(res=self.level.layer.grid[y][x])
                point = Point(x*res.width, y*res.height)
                self.level.layer.grid[y][x] = Tile(res.imagePathBuffer, pos=point, height=res.height, width=res.width)

    def draw(self, screen): #viewport):
        for y in range(self.col):
            for x in range(self.row):
                self.level[y][x].draw(screen)
